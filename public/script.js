const input = document.getElementById("input");
const rankButton = document.getElementById("rank-button");
const rankDialogContainer = document.getElementById("rank-dialog-container");
const rankButtons = document.getElementById("rank-buttons");
const rankA = document.getElementById("rank-a");
const rankB = document.getElementById("rank-b");
const output = document.getElementById("output");

const rankCache = new Map();

function rank(a, b) {
	if (rankCache.has([a, b])) return rankCache.get([a, b]);
	rankA.textContent = a;
	rankB.textContent = b;
	rankDialogContainer.classList.remove("closed");
	return new Promise((resolve, _) => {
		rankButtons.addEventListener(
			"submit",
			(e) => {
				e.preventDefault();
				rankDialogContainer.classList.add("closed");
				switch (e.submitter.id) {
					case rankA.id:
						rankCache.set([a, b], true);
						return resolve(true);
					case rankB.id:
						rankCache.set([a, b], false);
						return resolve(false);
					default:
						return reject();
				}
			},
			{ once: true }
		);
	});
}

async function quicksort(array) {
	if (array.length <= 1) {
		return array;
	}

	const pivot = array.splice(Math.floor(Math.random() * array.length), 1);

	const left = [];
	const right = [];

	for (item of array) {
		(await rank(item, pivot)) ? left.push(item) : right.push(item);
	}

	return (await quicksort(left)).concat(pivot, await quicksort(right));
}

rankButton.addEventListener("click", async () => {
	rankCache.clear();
	const ranking = input.value.trim().split("\n");
	const sorted = await quicksort(ranking);
	output.innerHTML = "";
	for (el of sorted) {
		output.innerHTML += `<li>${el}</li>`;
	}
});
